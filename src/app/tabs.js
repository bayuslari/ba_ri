export const Tabs = () => {
    // Tabs
    const main = document.querySelector(".main");
    const tabsBtns = main.querySelectorAll(".tabs__btn");
    const tabsContents = main.querySelectorAll(".tabs__content");

    const addClass = (i) => {
        tabsBtns[i].classList.add('active');
    }

    const removeClass = (i) => {
        tabsBtns[i].classList.remove('active');
    }

    function displayCurrentTab(current) {
        for (let i = 0; i < tabsContents.length; i++) {
            tabsContents[i].style.display = (current === i) ? "block" : "none";
            (current === i) ? tabsBtns[i].classList.add('active') : tabsBtns[i].classList.remove('active');
        }
    }
    displayCurrentTab(0);

    main.addEventListener("click", event => {
        if (event.target.className === "tabs__btn") {
            for (let i = 0; i < tabsBtns.length; i++) {
                if (event.target === tabsBtns[i]) {
                    displayCurrentTab(i);
                    addClass(i);
                    break;
                }
            }
        }
    });
    // End Tabs
}