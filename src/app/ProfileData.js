export const ProfileData = () => {
    let el = document.getElementById('profile__data2');
    let profileData = ['Jessica Parker', 'www.seller.com', '(949) 325 - 68594', 'Newport Beach, CA'];
    let labelData = ['YOUR NAME', 'YOUR WEBSITE', 'YOUR PHONE', 'CITY STATE & ZIP'];

    // Header profile
    let name = document.querySelector('.profile__name');
    let loc = document.querySelector('.profile__loc span');
    let tel = document.querySelector('.profile__tel span');

    const fetchData1 = () => {
        name.innerHTML = profileData[0];
        loc.innerHTML = profileData[3];
        tel.innerHTML = profileData[2];
    }

    fetchData1();

    const fetchData2 = (arr) => {
        let data2 = '';

        if (arr.length > 0) {
            for (let i = 0; i < arr.length; i++) {

                data2 += '<tr class="data__row">';
                data2 += '<td class="data__item">' + arr[i] + '</td>';
                data2 += '<td class="data__btn"><button class="edit__data""><svg width="15" id="pencil-sha' +
                        'rp" class="ionicon" viewBox="0 0 512 512"><path d="M103 464H48v-55L358.14 98.09l' +
                        '55.77 55.78L103 464zM425.72 142L370 86.28l31.66-30.66C406.55 50.7 414.05 48 421 ' +
                        '48a25.91 25.91 0 0118.42 7.62l17 17A25.87 25.87 0 01464 91c0 7-2.71 14.45-7.62 1' +
                        '9.36zm-7.52-70.83z"></path></svg></button></td>';
                data2 += '<td><div class="spoiler">'
                data2 += '<form action="javascript:void(0);" method="POST" class="spoiler__form">'
                data2 += '<label>' + labelData[i] + '</label>'
                data2 += '<input type="text" name="input__profile" class="input__edit" value="' + arr[i] + '">'
                data2 += '<div class="spoiler__btn">'
                data2 += '<button class="submit__edit" type="submit">SAVE</button>'
                data2 += '<button class="cancel__edit">Cancel</button>'
                data2 += '</div>'
                data2 += '</form>'
                data2 += '</td>'
                data2 += '</tr>';
            }
        }
        return el.innerHTML = data2;
    }

    fetchData2(profileData);

    const Edit = () => {
        let btnEdit = document.querySelectorAll('.edit__data');
        let saveEdit = document.querySelectorAll('.submit__edit');
        let inputEdit = document.querySelectorAll('.input__edit');
        let cancelEdit = document.querySelectorAll('.cancel__edit');
        let dataItem = document.querySelectorAll('.data__item');
        let spoiler = document.querySelectorAll('.spoiler');

        const showSpoiler = (current) => {
            for (let i = 0; i < spoiler.length; i++) {
                spoiler[i].style.display = (current === i)
                    ? "block"
                    : "none";
            }
        }

        const submitEdit = (i) => {
            dataItem[i].innerHTML = inputEdit[i].value;

            // update profile data in header
            if (i === 0) {
                name.innerHTML = inputEdit[i].value;
            } else if (i === 2) {
                tel.innerHTML = inputEdit[i].value;
            } else if (i === 3) {
                loc.innerHTML = inputEdit[i].value;
            }

            hideSpoiler(i);
        }

        const hideSpoiler = (i) => {
            spoiler[i].style.display = "none";
        }

        document.addEventListener("click", e => {
            if (e.target.className === "edit__data") {
                for (let i = 0; i < btnEdit.length; i++) {
                    if (e.target === btnEdit[i]) {
                        el.value = profileData[i];
                        showSpoiler(i);
                    }
                }
            }
            if (e.target.className === "submit__edit") {
                for (let i = 0; i < saveEdit.length; i++) {
                    if (e.target === saveEdit[i]) {
                        submitEdit(i);
                        break;
                    }
                }
            }

            if (e.target.className === "cancel__edit") {
                for (let i = 0; i < cancelEdit.length; i++) {
                    if (e.target === cancelEdit[i]) {
                        hideSpoiler(i)
                        break;
                    }
                }
            }
        })
    }

    Edit();

    const Responsive = () => {
        let toggleBtnMob = document.getElementById('toggle__btn-mob');
        let profileData = document.getElementById('profile__data2');
        let inputProfile = document.querySelectorAll('.input__edit');
        let dataItem = document.querySelectorAll('.data__item');

        const getValues = () => {
            for (let i = 0; i < inputProfile.length; i++) {
                let val = inputProfile[i].value;
                dataItem[i].innerHTML = val;

                // update profile data in header
                if (i === 0) {
                    name.innerHTML = inputProfile[i].value;
                } else if (i === 2) {
                    tel.innerHTML = inputProfile[i].value;
                } else if (i === 3) {
                    loc.innerHTML = inputProfile[i].value;
                }
            }
        }

        const handleShowMob = () => {
            profileData
                .classList
                .toggle("show--input-mob");
            toggleBtnMob
                .classList
                .toggle("show--toggle");
        }

        const handleHideMob = () => {
            profileData
                .classList
                .remove("show--input-mob");
            toggleBtnMob
                .classList
                .remove("show--toggle");
        }

        document.addEventListener("click", e => {
            if (e.target.className === "edit__mobile") {
                handleShowMob();
            }
            if (e.target.id === "cancel__mob") {
                handleHideMob();
            }
            if (e.target.id === "save__mob") {
                getValues();
                handleHideMob();
            }

        })
    }

    Responsive();
}